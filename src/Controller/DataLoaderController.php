<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Categories;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DataLoaderController extends AbstractController
{
    /**
     * @Route("/data", name="data_loader")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $file_products = dirname(dirname(__DIR__)) . "\product.json";
        $data_products = json_decode(file_get_contents($file_products));
        $file_categories = dirname(dirname(__DIR__)) . "\categories.json";
        $data_categories = json_decode(file_get_contents($file_categories));
        $categories = [];
        foreach ($data_categories as $data_category) {
            $category = new Categories();
            $category->setName($data_category->name)
                     ->setImage($data_category->image)
                     ->setDescription($data_category->description);
            $entityManager->persist($category);
            $categories[] = $category;
        }
        foreach ($data_products as $data_product) {
            $product = new Product();
            $product->setName($data_product->name)
                    ->setDescription($data_product->description)
                    ->setPrice($data_product->price)
                    ->setIsBestSeller($data_product->is_best_seller)
                    ->setIsNewArrival($data_product->is_new_arrival)
                    ->setIsFeatured($data_product->is_featured)
                    ->setIsSpecialOffer($data_product->is_special_offer)
                    ->setImage($data_product->image)
                    ->setQuantity($data_product->quantity)
                    ->setTags($data_product->tags)
                    ->setSlug($data_product->slug)
                    ->setCreatedAt(new \DateTime());
            $entityManager->persist($product);
            $products[] = $product;
        }
        //$entityManager->flush();
        return $this->json([
            'message' => 'Welcome to your new DataController! Yours data are uploaded.',
            'path' => 'src/Controller/DataLoaderController.php',
        ]);
    }
}
